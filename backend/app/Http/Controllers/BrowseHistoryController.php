<?php

namespace App\Http\Controllers;

use App\History;
use App\Visitor;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class BrowseHistoryController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $this->validate($request, [
            'url' => 'max:500',
            'footprint_value'=>'max:32',
            'cookie_value'=>'max:32'
        ]);

        $footprint="";
        $cookie = "";

        if(array_key_exists('footprint_value',$input)){
            $footprint =  Visitor::where('footprint_value', $input['footprint_value'])->first();
        }
        if(array_key_exists('cookie_value',$input)) {
            $cookie = Visitor::where('cookie_value', $input['cookie_value'])->first();
        }
        $history['url'] = $input['url'];
        $history['visited'] = Carbon::now()->toDateTimeString();

        if($cookie){
            $history['fi_visitor']= $cookie['id'];
            return History::create($history);
        }else if($footprint){
            $history['fi_visitor']= $footprint['id'];
            return History::create($history);
        }else{
            return Response::json(array(
                'status'=>400,
                'msg'=> 'Invalid Cookie & Fingerprint'
            ));
        }
    }


    public function show(Request $request){
        $input = $request->all();

        if (array_key_exists('cookie_value',$input) && $input['cookie_value']){
            $res =  Visitor::where('cookie_value', $input['cookie_value'])->first();
            return History::where('fi_visitor',$res['id'])->orderBy('visited', 'desc')->get();
        }else if (array_key_exists('footprint_value',$input) && $input['footprint_value']){
            $res =  Visitor::where('footprint_value', $input['footprint_value'])->first();
            return History::where('fi_visitor',$res['id'])->orderBy('visited', 'desc')->get();
        }else{
            return History::distinct()->get();
        }
    }



}
