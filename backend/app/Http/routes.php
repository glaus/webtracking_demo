<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'api/', 'middleware' => 'cors' ], function(){

    /*
     * Visitor Route
     */
    Route::post('visitor',[ 'uses' => 'VisitorController@store']);
    Route::get('visitor', [ 'uses' => 'VisitorController@show']);
    Route::put('visitor/cookie', [ 'uses' => 'VisitorController@updateByCookie']);
    Route::put('visitor/footprint', [ 'uses' => 'VisitorController@updateByFootprint']);

    Route::post('history',[ 'uses' => 'BrowseHistoryController@store']);
    Route::get('history', [ 'uses' => 'BrowseHistoryController@show']);

    Route::put('migrate', function () {

        \Illuminate\Support\Facades\Artisan::call('migrate');
        return \Illuminate\Support\Facades\Artisan::output();
    });

});

