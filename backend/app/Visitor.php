<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{

    protected $table = 'tbl_visitor';
    protected $fillable = array('cookie_value', 'footprint_value');
}
