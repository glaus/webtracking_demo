<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url',500);
            $table->dateTime('visited');
            $table->integer('fi_visitor')->unsigned();
            $table->timestamps();

            $table
                ->foreign('fi_visitor')
                ->references('id')
                ->on('tbl_visitor')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
