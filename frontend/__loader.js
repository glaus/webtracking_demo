(function(){

    if (!window.angular) {
        //Angular
        document.write('<script src="node_modules/angular/angular.js"><\/script>');
        document.write('<script src="node_modules/angular-animate/angular-animate.js"><\/script>');
        document.write('<script src="node_modules/angular-ui-router/release/angular-ui-router.js"><\/script>');
        document.write('<script src="node_modules/angular-aria/angular-aria.js"><\/script>');
        document.write('<script src="node_modules/angular-material/angular-material.js"><\/script>');
        document.write('<script src="lib/angular-cookie/angular-cookie.js"><\/script>');

        //Fingerprint
        document.write('<script src="node_modules/fingerprintjs2/dist/fingerprint2.min.js"><\/script>');

        //Easy XDM
        document.write('<script src="lib//easyXDM/easyXDM.debug.js"><\/script>');

        //Evercookie
        document.write('<script src="lib/evercookie/js/evercookie.js"><\/script>');
        document.write('<script src="lib/evercookie/js/swfobject-2.2.min.js"><\/script>');
        document.write('<script src="http://www.java.com/js/dtjava.js"><\/script>');


        //Application
        document.write('<script src="js/fullComprehensive/fullComprehensive.js"><\/script>');
        document.write('<script src="js/fullComprehensive/fullComprehensiveController.js"><\/script>');
        document.write('<script src="js/home/Home.js"><\/script>');
        document.write('<script src="js/home/HomeController.js"><\/script>');
        document.write('<script src="js/easyXDM/easyXDM.js"><\/script>');
        document.write('<script src="js/easyXDM/easyXDMController.js"><\/script>');
        document.write('<script src="js/history/History.js"><\/script>');
        document.write('<script src="js/history/HistoryController.js"><\/script>');
        document.write('<script src="js/services/Tracking.js"><\/script>');
        document.write('<script src="js/app.js"><\/script>');
    }

})();
