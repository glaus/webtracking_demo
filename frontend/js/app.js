



(function(){
    'use strict';

    // Prepare the 'users' module for subsequent registration of controllers and delegates
    angular
        .module('starterApp', [
            //Libs
            'ngMaterial',
            'ui.router',
            'ipCookie',

            //Personal Modules
            'home',
            'fullComprehensive',
            'easyXDM',
            'services',
            'history'
        ])
})();
