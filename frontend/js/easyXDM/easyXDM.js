(function(){
    'use strict';

    // Prepare the 'users' module for subsequent registration of controllers and delegates
    angular.module('easyXDM', [ 'ngMaterial' ])
        .config(homeRouteProvider);

    function homeRouteProvider($stateProvider) {
        $stateProvider
            .state("easyXDM", {
                views: {
                    "content" : {
                        templateUrl: "js/easyXDM/easyXDM.html",
                        controller: "EasyXDMController"
                    }
                },
                url:"/easyXDMListener"
            });
    };



})();
