(function(){

    angular
        .module('easyXDM')
        .controller('EasyXDMController', [ 'ipCookie','Tracking',
            EasyXDMController
        ]);

    /**
     * Main Controller for the Angular Material Starter App
     * @param $scope
     * @param $mdSidenav
     * @param avatarsService
     * @constructor
     */
    function EasyXDMController(ipCookie, Tracking) {
        var self = this;

        //Load Easy XDM
        loadEasyXdm();

        // *********************************
        // Internal methods
        // *********************************

        /**
         * Hide or Show the 'left' sideNav area
         */
        function loadEasyXdm(){
            var socket = new easyXDM.Socket({
                onMessage:response
            });
        }

        function response(message, origin){
            Tracking.addPage(message);
        }

    }

})();
