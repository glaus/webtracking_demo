(function(){
    'use strict';

    // Prepare the 'users' module for subsequent registration of controllers and delegates
    angular.module('fullComprehensive', [ 'ngMaterial' ])
        .config(homeRouteProvider);

    function homeRouteProvider($stateProvider) {
        $stateProvider
            .state("fullComprehensive", {
                views: {
                    "content" : {
                        templateUrl: "js/fullComprehensive/fullComprehensive.html",
                        controller: "FullComprehensiveController"
                    }
                },
                url:"/fullComprehensive"
            });
    };



})();
