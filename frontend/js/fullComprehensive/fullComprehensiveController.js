(function(){

    angular
        .module('fullComprehensive')
        .controller('FullComprehensiveController', [
            '$scope', 'Tracking',
            FullComprehensiveController
        ]);

    /**
     * Main Controller for the Angular Material Starter App
     * @param $scope
     * @param $mdSidenav
     * @param avatarsService
     * @constructor
     */
    function FullComprehensiveController($scope, Tracking) {
        var self = this;
        Tracking.addPage("fabianglaus.ch/tracker/#/fullComprehensive");
    }

})();
