(function(){

    angular
        .module('history')
        .controller('HistoryController', [
            '$scope', 'Tracking',
            HistoryController
        ]);

    /**
     * Main Controller for the Angular Material Starter App
     * @param $scope
     * @param $mdSidenav
     * @param avatarsService
     * @constructor
     */
    function HistoryController($scope, Tracking) {
        var self = this;
        Tracking.addPage("fabianglaus.ch/tracker/#/history");

        Tracking.listHistory(callback);

        function callback(data){
            if (data.length > 0){
                $scope.history = data;
            }else{
                $scope.history = [
                    {
                        url: "No History available"
                    }
                ]
            }

        }
    }

})();
