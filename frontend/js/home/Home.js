(function(){
    'use strict';

    // Prepare the 'users' module for subsequent registration of controllers and delegates
    angular.module('home', [ 'ngMaterial' ])
        .config(homeRouteProvider);

    function homeRouteProvider($stateProvider) {
        $stateProvider
            .state("home", {
                views: {
                    "content" : {
                        templateUrl: "js/home/home.html",
                        controller: "HomeController"
                    }
                },
                url:"/"
            });
    };



})();
