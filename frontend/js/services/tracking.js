(function() {
    "use strict";

    angular
        .module("services",[])
        .factory("Tracking", ["$http",trackingService]);

    /**
     * Auth service, handles simple login
     */
    function trackingService($http) {


        var apiURL = 'http://fabianglaus.ch/api/';

        var TrackingService;
        TrackingService = {
            addPage: addPage,
            cookie: cookieGenerator,
            listHistory: listHistory
        };

        var optEverCookie = {
            history: false, // CSS history knocking or not .. can be network intensive
            java: false, // Java applet on/off... may prompt users for permission to run.
            tests: 10,  // 1000 what is it, actually?
            silverlight: true, // you might want to turn it off https://github.com/samyk/evercookie/issues/45
            domain: '.' + window.location.host.replace(/:\d+/, ''), // Get current domain
            baseurl: '/tracker', // base url for php, flash and silverlight assets
            asseturi: '/lib/evercookie/assets', // assets = .fla, .jar, etc
            phpuri: '/lib/evercookie/php', // php file path or route
            authPath: false, //'/evercookie_auth.php', // set to false to disable Basic Authentication cache
            pngCookieName: '',
            pngPath: '/evercookie_png.php',
            etagCookieName: '',
            etagPath: '/evercookie_etag.php',
            cacheCookieName: '',
            cachePath: '/evercookie_cache.php',
            hsts: false,
            hsts_domains: []
        };
        var optFingerprint = {
            extendedFontList: true,
            excludeAvailableScreenResolution: true,
            excludeIndexedDB: true
        };


        var ec = new evercookie(optEverCookie);


        function entityToChar(entity) {
            var s = document.createElement('span');
            s.innerHTML = entity;
            return s.innerHTML;
        }



        function cookieGenerator(){
            function randomString(length, chars) {
                var result = '';
                for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
                return result;
            }
            return randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        }

        function prettyFingerprint(arrFootprint){
            var res = [];
            angular.forEach(arrFootprint, function(value, key) {
                this[value['key']] = value['value'];
            }, res);
            console.log(arrFootprint);
            //console.log(res);     NOT RUNNING YET
        }


        function addPage(url, callback) {


            /*
            Check if the User has a valid Cookie
             */
            console.log("---------------(1) Check Cookie---------------");
            ec.get("_tracker", function(cookie, allCookie) {
                if(cookie){
                    $http({
                        method: 'GET',
                        url: apiURL+'visitor?cookie_value='+cookie,
                    }).success(function(data){
                        if(data){
                            /*
                            Cookie is Valid
                             */
                            console.log(entityToChar("&#9989;")+"       Found Cookie");
                            console.log(cookie);
                            console.log(allCookie);

                            new Fingerprint2(optFingerprint).get(function(footprint, allFootprint){
                                $http({
                                    method: 'PUT',
                                    url: apiURL+'visitor/cookie',
                                    data:{
                                        footprint_value: footprint,
                                        cookie_value: cookie
                                    }
                                }).success(function(data){
                                    ec.set("_tracker", cookie);
                                    console.log(entityToChar("&#9989;")+"       Updated Fingerprint of User");
                                    console.log(footprint);
                                    prettyFingerprint(allFootprint);
                                    console.log("----------------------------------------------");
                                    /*
                                    Variante Add Page by Cookie
                                     */
                                    console.log("-----------------(2) Log Page-----------------");
                                    console.log("Logging the request using the Cookie");
                                    addPage(cookie,footprint);
                                }).error(function(){
                                    console.log(entityToChar("&#10060;")+"       Fingerprint could not be updated");
                                });
                            });
                        }else{
                            console.log(entityToChar("&#10060;")+"       Unknown Cookie available");
                            console.log("----------------------------------------------");
                            checkFootprint(callback);
                        }
                    }).error(function(){
                        console.log(entityToChar("&#10060;")+"       Cookie could not be checked");
                    });
                }else{
                    console.log(entityToChar("&#10060;")+"       No Cookie is available");
                    console.log("----------------------------------------------");
                    checkFootprint(callback);
                }
            });

            /*
            Check if the User has a valid Footprint
             */
            function checkFootprint(){
                var cookie = cookieGenerator();
                console.log("-------------(2) Check Fingerprint------------");
                new Fingerprint2(optFingerprint).get(function(footprint, allFootprint){
                    //Check if the footprint is allready in the DB
                    console.log(entityToChar("&#9989;")+"       Generated the Fingerprint of the Browser");
                    console.log(footprint);
                    prettyFingerprint(allFootprint);
                    $http({
                        method: 'GET',
                        url: apiURL+'visitor?footprint_value='+footprint,
                    }).success(function(data){
                        if(data){
                            console.log(entityToChar("&#9989;")+"       Fingerprint is valid");


                            $http({
                                method: 'PUT',
                                url: apiURL+'visitor/footprint',
                                data:{
                                    footprint_value: footprint,
                                    cookie_value: cookie
                                }
                            }).success(function(data){
                                ec.set("_tracker", cookie);
                                console.log(entityToChar("&#9989;")+"       Created new Cookie");
                                console.log(cookie);
                                console.log("----------------------------------------------");
                                console.log("");
                                console.log("-----------------(3) Log Page-----------------");
                                console.log("Logging the request using the Cookie");
                                addPage(cookie,footprint);
                            }).error(function(){
                                console.log(entityToChar("&#10060;")+"       Cookie could not be updated");
                            });

                            //Unknown Footprint add new User
                        }else{
                            /*
                             New Footprint
                             */
                            console.log(entityToChar("&#10060;")+"       Could not find Fingerprint in Database");
                            console.log(footprint);
                            prettyFingerprint(allFootprint);
                            $http({
                                method: 'POST',
                                url: apiURL+'visitor',
                                data:{
                                    footprint_value: footprint,
                                    cookie_value: cookie
                                }
                            }).success(function(data){
                                ec.set("_tracker", cookie);
                                console.log(entityToChar("&#9989;")+"       Added new Visitor to the Database");
                                console.log("-----------------(3) Log Page-----------------");
                                console.log("Logging the request using the Cookie");
                                addPage(cookie,footprint);

                            }).error(function(){
                                console.log(entityToChar("&#10060;")+"       Could not add Visitor to the Database");
                            });
                        }
                    }).error(function(){
                        console.log(entityToChar("&#10060;")+"       Did not find a Visitor by Footprint");
                    });

                });
                console.log("----------------------------------------------");
            }

            function addPage(cookie, footprint){
                $http({
                    method: 'POST',
                    url: apiURL+'history',
                    data:{
                        url: url,
                        footprint_value: footprint,
                        cookie_value: cookie
                    }
                }).success(function(data){
                    console.log(entityToChar("&#9989;")+"       Page added to History");
                    console.log("----------------------------------------------");
                }).error(function(){
                    console.log("ERROR: Insert History")
                });
            }
        }

        function listHistory(callback){
            ec.get("_tracker", function(cookie) {
                new Fingerprint2(optFingerprint).get(function(footprint){

                    if(cookie){
                        $http({
                            method: 'GET',
                            url: apiURL+'history?cookie_value='+cookie,
                        }).success(function(data){
                            callback(data);

                        }).error(function(){
                            callback("ERROR: Insert History")
                        });
                    }else{
                        $http({
                            method: 'GET',
                            url: apiURL+'history?footprint_value='+footprint,
                        }).success(function(data){
                            callback(data);

                        }).error(function(){
                            callback("ERROR: Insert History")
                        });
                    }


                });
            });
        }

        return TrackingService;
    }

})();
